//
//  ViewController.swift
//  test22
//
//  Created by jatin verma on 2019-11-07.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import UIKit
import Particle_SDK
class ViewController: UIViewController {
    
    
    
    // MARK: User variables
    let USERNAME = "jatin_verma@outlook.com"
    let PASSWORD = "kaur1234"
    
    // MARK: Device
    
    let DEVICE_ID = "3f003b001047363333343437"
    var myPhoton : ParticleDevice?

    
    
    // variables
    
    var counter = -1
    var timer = Timer()
var totalTime = 20
    var slide = 1
   var v = 0
    
    @IBOutlet weak var variableLable: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
       // Particle Setup
        ParticleCloud.init()
        
               // 2. Login to your account
               ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
                   if (error != nil) {
                       // Something went wrong!
                       print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                       // Print out more detailed information
                       print(error?.localizedDescription)
                   }
                   else {
                       print("Login success!")

                       // try to get the device
                       self.getDeviceFromCloud()

                   }
               } // end login
        
    }
    //Timer
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBAction func slowdown(_ sender: UISlider) {
        self.v = Int(sender .value)
        self.variableLable.text = String(v)
        if (self.v > 0 && self.v < 20) {
            slide = 2
        }
        else if (self.v > 20 && self.v < 50) {
                   slide = 5
               }
        else if (self.v > 50 && self.v < 80) {
            slide = 8
        }
        else if (self.v > 80 && self.v < 100) {
            slide = 10
        }
    }
    @IBAction func startTimerButton(_ sender: Any) {
    timer.invalidate()
    // start the timer
        
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(self.slide), target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
       
    }

    // called every time interval from the timer
    @objc func timerAction() {
        if (counter > -2 && counter < totalTime) {
        counter += 1
        timerLabel.text = "\(counter)"
        }
        else if (counter == totalTime ) {
            timerLabel.text = "Stopped"
        }
    }
// timer end
    
// particle Functions
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                DispatchQueue.main.async {
               
            if let _ = error {
                print("could not subscribe to events")
            }
            else{
                print("got event with data \(event?.data)")
                var choice = (event?.data)!
                if (choice == "0" ) {
                    self.turnParticleGreen()
                   
             
                }
                else if (choice == "7") {
                      self.turnParticleBlue()
                }
                    else if (choice == "10") {
                    self.turnParticleYellow()
                }
            else if (choice == "15") {
                               self.turnParticleOrange()
                           }
        else if (choice == "20") {
            self.turnParticleRed()
        }
                    }
           
                }
        })
    }
    
    func turnParticleGreen() {
        
        print("Pressed the change lights button")
        
        let parameters = [self.counter] as [Any]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    func turnParticleBlue() {
           
           print("Pressed the change lights button")
           
           let parameters = [self.counter] as [Any]
           var task = myPhoton!.callFunction("answer", withArguments: parameters) {
               (resultCode : NSNumber?, error : Error?) -> Void in
               if (error == nil) {
                   print("Sent message to Particle to turn green")
               }
               else {
                   print("Error when telling Particle to turn green")
               }
           }
           //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
           
       }
    func turnParticleYellow() {
           
           print("Pressed the change lights button")
           
           let parameters = [self.counter] as [Any]
           var task = myPhoton!.callFunction("answer", withArguments: parameters) {
               (resultCode : NSNumber?, error : Error?) -> Void in
               if (error == nil) {
                   print("Sent message to Particle to turn green")
               }
               else {
                   print("Error when telling Particle to turn green")
               }
           }
           //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
           
       }
    func turnParticleOrange() {
           
           print("Pressed the change lights button")
           
           let parameters = [self.counter] as [Any]
           var task = myPhoton!.callFunction("answer", withArguments: parameters) {
               (resultCode : NSNumber?, error : Error?) -> Void in
               if (error == nil) {
                   print("Sent message to Particle to turn green")
               }
               else {
                   print("Error when telling Particle to turn green")
               }
           }
           //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
           
       }
    func turnParticleRed() {
        
        print("Pressed the change lights button")
        
        let parameters = [self.counter] as [Any]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }

}


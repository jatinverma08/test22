// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

#include "math.h"


InternetButton button = InternetButton();
void setup() {
  button.begin();

  // Exposed functions-test


  // http://api.particle.io/<deviceid>/answer
  Particle.function("answer", smile);

  // Show a visual indication to the player that the Particle
  // is loaded & ready to accept inputs
  for (int i = 0; i < 3; i++) {
    button.allLedsOn(20,0,0);
    delay(250);
    button.allLedsOff();
    delay(250);
  }
}

int smile(String cmd) {
  if (cmd == "green") {
    button.allLedsOn(0,255,0);
    delay(2000);
    button.allLedsOff();
  }
  else if (cmd == "blue") {
    button.allLedsOn(76, 132, 237);
    delay(2000);
    button.allLedsOff();
  } 
  else if (cmd == "yellow") {
    button.allLedsOn(235, 221, 75);
    delay(2000);
    button.allLedsOff();
  }
   else if (cmd == "orange") {
    button.allLedsOn(230, 128, 32);
    delay(2000);
    button.allLedsOff();
  }
  
  else if (cmd == "red") {
    button.allLedsOn(255,0,0);
    delay(2000);
    button.allLedsOff();
  }
  else {
    // you received an invalid color, so
    // return error code = -1
    return -1;
  }
  // function succesfully finished
  return 1;
}

int DELAY = 200;

void loop() {


// Mandatory

  // EVENT 1:
  // when person presses button 4 & 2 send player choice to phone
  if (button.buttonOn(1)) {
    // CHOICE = A
    Particle.publish("playerChoice","counter", 60, PRIVATE);
    delay(DELAY);
  }
  
  
  
  
}
  



